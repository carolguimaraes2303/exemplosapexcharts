import Vue from 'vue'
import VueRouter from 'vue-router'
import Apexchart1 from '../components/Apexchart1.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Exemplo1',
    component: Apexchart1
  },
  {
    path: '/Exemplo2',
    name: 'Exemplo2',
    component: () => import('../components/Apexchart2.vue')
  },
  {
    path: '/Exemplo3',
    name: 'Exemplo3',
    component: () => import('../components/Apexchart3.vue')
  },
  {
    path: '/Exemplo4',
    name: 'Exemplo4',
    component: () => import('../components/Apexchart4.vue')
  },
  {
    path: '/Exemplo5',
    name: 'Exemplo5',
    component: () => import('../components/Apexchart5.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
